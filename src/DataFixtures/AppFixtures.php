<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\User;
use App\Entity\Subscription;
use App\Entity\Card;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        // Fixtures Subscription
        for ($i = 0; $i < 10; $i++) {
            $sub = new Subscription();
            $sub->setName('sub '.$i);
            $sub->setSlogan("Slogan".$i);
            $sub->setUrl("www.".$i.".com");
            $manager->persist($sub);
        }

        // Fixtures Users
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setFirstname("firstname".$i);
            $user->setLastname("lastname".$i);
            $user->setEmail("mail".$i."@mail.com");
            $user->setApiKey(rand(1,10000));
            $user->setCreatedAt(new \DateTime());
            $user->setAdress("Mon adresse".$i);
            $user->setCountry("Mon pays".$i);
            $user->setSubscription($i);
            $manager->persist($user);
        }


        // Fixtures Card
        for ($i = 0; $i < 10; $i++) {
            $card = new Card();
            $card->setName("Name".$i);
            $card->setCreditCardType("Type".$i);
            $card->setCreditCardNumber(rand(1,100000));
            $card->setCurrencyCode("EUR");
            $card->setValue(1000);
            $card->setUser();
            
            $manager->persist($card);
        }

        

        

        $manager->flush();
    }
}
