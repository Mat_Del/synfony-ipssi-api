<?php

namespace App\Controller;

use App\Entity\User;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use JMS\Serializer\SerializationContext;


class UserController extends AbstractFOSRestController
{
    private $userRepository;
    public function __construct(UserRepository $userRepository, EntityManagerInterface $em)
    {
        $this->userRepository = $userRepository ;
        $this->em = $em;
    }

    /** 
     * @\JMS\Serializer\Annotation\MaxDepth(1)
     *  @Rest\Get("/api/users/{email}")     
    */
    public function getApiUser(User $user){
        
        return $this->view($user);
    }
    
    /**     
     * @Rest\Get("/api/users")
    */
    public function getApiUsers(){
        $users = $this->userRepository ->findAll();
        return $this->view($users);
    }
    
    /**
     * @Rest\Post("/api/users")
     * @ParamConverter("user", converter="fos_rest.request_body")
    */
    public function postApiUser(User $user){
        $this->em->persist($user);
        $this->em->flush();
        return $this->view($user);
    }
    
    /**    
     * @Rest\Patch("/api/users/{email}")
    */
    public function patchApiUser(User $user){}
    
    /**    
     * @Rest\Delete("/api/users/{email}")
    */
    public function deleteApiUser(User $user, EntityManagerInterface $em){
        // $user = $em->getId();
        // if($user !== null){
        //     $entityManager->remove($user);
        //     $entityManager->flush();
        //     return $this->view();
        // } else {
        //     throw $this->createNotFoundException('There are no user with this id');
        // }
    }
}


