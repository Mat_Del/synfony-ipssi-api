<?php

namespace App\Controller;

use App\Entity\Card;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use App\Repository\CardRepository;
use JMS\Serializer\SerializationContext;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use FOS\RestBundle\Controller\Annotations as Rest;


class CardController extends AbstractFOSRestController
{
    private $CardRepository;
    public function __construct(CardRepository $CardRepository, EntityManagerInterface $em)
    {
        $this->CardRepository = $CardRepository ;
        $this->em = $em;
    }

    /**    
     * @IsGranted("ROLE_USER")   
     * @Rest\Get("/api/card/{id}")
     * @Rest\View(serializerGroups={"Card"})  
    */
    public function getApiCard(Card $subscription){
        return $this->view($subscription);
    }
    
    /**     
     * @IsGranted("ROLE_ADMIN") 
     * @Rest\Get("/api/card")   
    */
    public function getApiCards(){
        $card = $this->CardRepository ->findAll();
        return $this->view($card);
    }
    
    /**    
     * @IsGranted("ROLE_ADMIN") 
     * @Rest\Post("/api/card")
     * @ParamConverter("card", converter="fos_rest.request_body")
     * @Rest\View(serializerGroups={"Card"}) 
    */
    public function postApiCard(Card $card){
        $this->em->persist($card);
        $this->em->flush();
        return $this->view($card);
    }
    
    /**    
     * @IsGranted("ROLE_ADMIN") 
     * @Rest\Patch("/api/subscription/{id}")
     * @Rest\View(serializerGroups={"Card"})     
    */
    public function patchApiCard(Card $card){}

    
    /**    
     * @Rest\Delete("/api/subscription/{id}")
    */
    public function deleteApiCard(Card $card, EntityManagerInterface $em){
        // $card = $em->getId();
        // if($card !== null){
        //     $entityManager->remove($card);
        //     $entityManager->flush();
        //     return $this->view();
        // } else {
        //     throw $this->createNotFoundException('There are no card with this id');
        // }
    }
}
